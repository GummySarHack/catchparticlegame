// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PraticeJam1Character.generated.h"

UCLASS(config=Game)
class APraticeJam1Character : public ACharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	//Current Health
	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = Health, meta = (AllowPrivateAccess = "true"))
		float CurrentHealth = 100.f;	
	
	//MAx Health
	UPROPERTY(EditAnywhere, BLueprintReadWrite, Category = Health, meta = (AllowPrivateAccess = "true"))
		float MaxHealth = 100.f;

protected:

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface


public:
	APraticeJam1Character();

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	float GetCurrentHealth() { return CurrentHealth; };
	void SetCurrentHealth(float newHealth) 
	{ 
		CurrentHealth = newHealth;
	};
	float GetMaxHealth() { return MaxHealth; };
};
